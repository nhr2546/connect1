from page_objects import PageObject, PageElement
class LogoutPage(PageObject):
    def check_page(self):
        return "connectR" in self.w.title

