from page_objects import PageObject, PageElement
class LoginPage(PageObject):
    login_link = PageElement(link_text="Login")
    def check_page(self):
        return "ConnectR" in self.w.title
    def click_login(self, loginpage):
        self.login_link.click()
        return loginpage.check_page()
    username_field = PageElement(id_ = "UserId")
    password_field = PageElement(id_ = "Password")
    def login(self, matterpage, username, password):
        self.username_field.send_keys(username)
        self.password_field.send_keys(password)
        self.password_field.submit()
        return matterpage.check_page()
