from page_objects import PageObject, PageElement
class MatterPage(PageObject):
    logout_link = PageElement(link_text="Logout")
    def check_page(self):
        return "connectR - Matters" in self.w.title
    def click_logout(self, matterpage):
        self.logout_link.click()
        return "connectR" in self.w.title
