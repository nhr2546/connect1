from page_objects import PageObject, PageElement
class MatterDetailsPage(PageObject):
    def check_page(self):
        return "connectR - Matter Details" in self.w.title
