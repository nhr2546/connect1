import Base_test
class TestCases(Base_test.BaseTest):
    def test_01_test_login_page_login(self):
        # Step 1:Click on Login and the Login Page Loads
        assert self.loginpage.click_login(self.loginpage)
        # Step 2: Login to the webpage and confirm the main page appears
        assert self.loginpage.login(self.matterpage, "rmc", "abcd")
        # Step 3: Logout of the webpage and confirm it returns to the login page
        assert self.matterpage.click_logout(self.logoutpage)
