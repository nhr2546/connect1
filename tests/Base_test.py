import unittest
from utils import Parameters
from Page20_login_page import LoginPage
from Page30_matter_page import MatterPage
from Page40_matter_details_page import MatterDetailsPage
from Page50_logout_page import LogoutPage


class BaseTest(unittest.TestCase):
    param = Parameters()
    loginpage = LoginPage(param.w, param.rootUrl)
    matterpage = MatterPage(param.w, param.rootUrl)
    matterdetailspage = MatterDetailsPage(param.w, param.rootUrl)
    logoutpage = LogoutPage(param.w, param.rootUrl)

    def setUp(self):
        self.param.w.get(self.param.rootUrl)
        self.param.w.maximize_window()
        assert self.loginpage.check_page()

    @classmethod
    def tearDownClass(cls):
        cls.param.w.quit()
